import urllib.request
import os
import time
from logconf import app_log
import subprocess

url = 'https://internet.iitb.ac.in'
google_url = "https://google.com"

filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "main.py") #should be an executable script
ping_interval = 0.5 #in minutes

while(True):

    #
    # try:
    #     urllib.request.urlopen(google_url, timeout=5)
    #     state = "connected"
    # except Exception as e:
    #     app_log.info(e)
    #
    #     try:
    #         urllib.request.urlopen(url, timeout=5)
    #         state = "insti"
    #     except Exception as e1:
    #         app_log.info(e1)
    #         state = "disconnected"

    os.system("python "+filename)
    # app_log.info(str(subprocess.check_output('python '+filename, stderr=subprocess.STDOUT, shell=True)))

    time.sleep(ping_interval*60)
