import scrapy
import os
from selenium import webdriver
import time
import urllib.request

def set_viewport_size(driver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    driver.set_window_size(*window_size)

dir_path = os.path.dirname(os.path.realpath(__file__))
par_dir = os.path.abspath(os.path.join(dir_path, os.pardir))
wallpaper_dir = os.path.join(par_dir, "Downloaded")

class QuotesDownloader(scrapy.Spider):
    name = "quotes"

    start_urls = [
        "https://quotefancy.com/"
    ]

    def extract_image_links(self, link):
        driver = webdriver.Chrome()
        set_viewport_size(driver, 1366, 768)
        driver.get(link)

        #delay and scroll through the page

        docheight = driver.execute_script('return document.body.scrollHeight')
        scrolltop = driver.execute_script('return $(window).scrollTop()')
        win_height = driver.execute_script('return $(window).height()')

        while(docheight > scrolltop + win_height):
            driver.execute_script('window.scrollTo(0,$(window).scrollTop()+$(window).height());')
            docheight = driver.execute_script('return document.body.scrollHeight')
            scrolltop = driver.execute_script('return $(window).scrollTop()')
            win_height = driver.execute_script('return $(window).height()')

            #click if more available
            try:
                loadmore = driver.find_element_by_class_name('loadmore')
                loadmore.click()
                time.sleep(1)
            except Exception:
                pass

            time.sleep(0.2)

        links = []
        imgs = driver.find_elements_by_tag_name('img')
        for img in imgs:
            links.append(img.get_attribute('src'))

        links = [x for x in links if ('wallpaper' in x and not 'thumb' in x)]

        driver.quit()

        return links


    def parse_author(self, response):

        dwnld_links = self.extract_image_links(response.url) # write code to extract links using selenium

        if(dwnld_links is not None):
            for link in dwnld_links:
                if(link is not None):
                    folder_path = os.path.join(wallpaper_dir, response.meta['folder'])
                    file_name = link.split('/')[-1]
                    urllib.request.urlretrieve(link, os.path.join(folder_path, file_name))

        pass

    def parse(self, response):

        title_uris = response.css('div[class="gridblock-title"] a::attr(href)').extract()

        if(title_uris is not None):
            for uri in title_uris:
                if(uri is not None):
                    folder_name = uri.split('/')[-1]
                    try:
                        os.mkdir(os.path.join(wallpaper_dir, folder_name))
                    except Exception as e:
                        print(e)
                    next_page = response.urljoin(uri)
                    req = scrapy.Request(next_page, callback=self.parse_author)
                    req.meta['folder'] = folder_name
                    yield req

        pass
