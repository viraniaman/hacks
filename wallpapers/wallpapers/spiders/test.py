from selenium import webdriver
import time

def set_viewport_size(driver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    driver.set_window_size(*window_size)

driver = webdriver.PhantomJS()
set_viewport_size(driver, 1366, 768)
driver.get('https://quotefancy.com/motivational-quotes')

#delay and scroll through the page

docheight = driver.execute_script('return document.body.scrollHeight')
scrolltop = driver.execute_script('return $(window).scrollTop()')
win_height = driver.execute_script('return $(window).height()')

clicker = 4

while(docheight > scrolltop + win_height):
    driver.execute_script('window.scrollTo(0,$(window).scrollTop()+$(window).height());')
    docheight = driver.execute_script('return document.body.scrollHeight')
    scrolltop = driver.execute_script('return $(window).scrollTop()')
    win_height = driver.execute_script('return $(window).height()')

    #click if more available
    try:
        if(clicker>0):
            clicker -= 1
            loadmore = driver.find_element_by_class_name('loadmore')
            loadmore.click()
            time.sleep(0.2)
    except Exception:
        pass

    time.sleep(0.2)

links = []
imgs = driver.find_elements_by_tag_name('img')
for img in imgs:
    links.append(img.get_attribute('src'))

links = [x for x in links if ('wallpaper' in x and not 'thumb' in x)]

print(links)
print(len(links))
