#!/usr/bin/env python
from playlist import *
import os
import pyperclip
import time
from tkinter import *
from tkinter import ttk
from database_connection import Song, db
from bs4 import BeautifulSoup
import urllib
import re

last_str = ""

"""

Have to set a timeout mechanism for selecting one particular link

"""

while(True):

    time.sleep(0.1)
    text = pyperclip.paste()
    text = re.search("(?P<url>https?://[^\s]+)", text).group("url")

    if(not (text == last_str)):

        flag = False
        for x in urls:
            if(x in text):
                flag = True
                break

        if(flag):
            root = Tk()
            app = App(root=root, text=text)
            last_str = text
            root.style = Style()
            root.style.theme_use('clam' )
            root.mainloop()
            db.close()
