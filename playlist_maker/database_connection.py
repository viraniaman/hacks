from peewee import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
db = SqliteDatabase(os.path.join(dir_path,'songs.db'))

class Song(Model):
    link = CharField()
    title = CharField(default="Could not get title from the webpage :(")

    class Meta:
        database = db
