#!/usr/bin/env python
"""
I am thinking what features I should add to this.
1. when link copied to clipboard, a pop up box should come which gives you option
of saving the song to the playlist.
2. background mp3 downloading of these songs
3. background upload of these mp3s to google drive

"""

import os
import pyperclip
import time
from tkinter import *
from tkinter.ttk import *
from tkinter import ttk, messagebox
from database_connection import Song, db
from bs4 import BeautifulSoup
import urllib
import webbrowser
import tkHyperlinkManager
import tkinter.font
import re


urls = ["youtube", "youtu.be"]


class DBView:

    """
    Widgets to be added:
    3. List of all songs in the format: name - link. But the entire thing is a link
    4. Textbox for search
    5. Shuffle button to randomly play one song

    """

    def open_link(self, link):
        print ("link clicked - "+str(link))
        webbrowser.open(link)

    def link_click_test(self, link):
        print("clicked on link: "+link)

    def __init__(self, root):

        self.root = root

        self.root.wm_title("Youtube songs db")

        self.songs = []
        db.connect()
        Song.create_table(True)

        search_frame = Frame(root)
        search_frame.grid(row=0, column=0)

        search_label = Label(search_frame, text="Search: ")
        search_label.grid(row=0, column=0, sticky="nsew")

        search_entry = StringVar()
        search_box = Entry(search_frame, textvariable=search_entry)
        search_box.grid(row=0, column=1, sticky="nsew")

        self.list_frame = Text(root, height=30, width=50)
        self.list_frame.grid(row=1, column=0)
        self.scroll = Scrollbar(root, command=self.list_frame.yview)
        self.scroll.grid(row = 1, column=1, sticky="ns")
        self.list_frame['yscrollcommand'] = self.scroll.set
        # self.list_frame.config(font=("consolas", 12), undo=True, wrap='word')


        # canvas = Canvas(self.list_frame)
        # canvas.grid(row=0,column=0,sticky="nsew")
        # frame = Frame(canvas)
        # frame.grid(row=0,column=0,sticky="nsew")
        # scroll = Scrollbar(self.list_frame, orient=VERTICAL, command=canvas.yview)
        # canvas.configure(yscrollcommand=scroll.set)
        # scroll.grid()

        self.set_list(search_entry.get())
        links = tkHyperlinkManager.HyperlinkManager(self.list_frame)
        search_box.bind('<KeyRelease>', lambda f: self.textbox_wrapper(search_entry.get(), self.list_frame, links))
        # row_num = 0
        # for song in self.songs:
        #     song.grid(row=row_num)
        #     row_num += 1

        search_box.focus_set()

        self.update_textbox(self.list_frame, links)


    def set_list(self, search_text):

        selected_songs = None

        if(not search_text):
            selected_songs = Song.select()
        else:
            selected_songs = Song.select().where(Song.title.contains(search_text) or Song.link.contains(search_text))

        self.songs = selected_songs

        # for song in selected_songs:
        #     element = str(song.title)
        #     element += ' - '
        #     element += str(song.link)
        #
        #     label = Label(parent, text=element, fg="blue", cursor="hand2")
        #     label.bind('<Button-1>', lambda: self.link_click_test(song.link))
        #
        #     self.songs.append(label)

        pass

    def update_textbox(self, textbox, links):

        textbox.delete(1.0, END)

        for song in self.songs:
            textbox.insert(INSERT, str(song.title) + " - ")
            textbox.insert(INSERT, str(song.link), links.add(lambda link=song.link: self.open_link(link)))
            textbox.insert(INSERT, "\n\n")

    def textbox_wrapper(self, text, textbox, links):

        self.set_list(text)
        self.update_textbox(textbox, links)



class App:

    def saveLink(self, link):

        now = time.time()
        song, created = Song.get_or_create(link=link)
        now1 = time.time()
        print("time taken to execute insert query: "+str((now1-now)/1000.0))

        self.root.destroy()

        if(created):
            soup = BeautifulSoup(urllib.request.urlopen(link).read(), "lxml")
            song.title = soup.title.string
            song.save()
            print("saved song in db")
        else:
            #show pop up here that this song already exists in db
            root = Tk()
            root.withdraw()
            messagebox.showinfo("Error", "Song already exists in database")
            root.destroy()
            pass

    def __init__(self, root, text):
        self.root = root
        db.connect()
        Song.create_table(True)

        """

        Widgets to be added:
        1. Textbox for the link
        2. Save button for the link

        """

        frame = Frame(root, padding="3 3 12 12", width=500, height=100)
        frame.grid(row=0, column=0, sticky="nsew")

        linkLabel = Label(frame, text = "Link:")
        linkLabel.grid(row=0,column=0)

        linkText = StringVar()
        linkText.set(text)
        linkTextBox = Entry(frame, textvariable=linkText)
        linkTextBox.grid(row=1,column=0,columnspan=2,sticky="ew")
        linkTextBox.bind('<Return>', lambda e: self.saveLink(linkText.get()))

        saveLinkButton = Button(frame, text="Save Song", command=lambda: self.saveLink(linkText.get()))
        saveLinkButton.grid(row=2,column=1)

        linkTextBox.focus_set()
        root.resizable(0,0)

if __name__ == "__main__":
    root = Tk()
    root.style = ttk.Style()
    root.style.theme_use('clam')

    default_font = tkinter.font.nametofont("TkDefaultFont")
    default_font.configure(family='clean', size=12)
    root.option_add("*Font", default_font)

    window = DBView(root=root)

    # get screen width and height
    ws = root.winfo_screenwidth() # width of the screen
    hs = root.winfo_screenheight() # height of the screen

    root.update_idletasks()

    w = root.winfo_width()
    h = root.winfo_height()

    # calculate x and y coordinates for the Tk root window
    x = (ws/2) - (w/2)
    y = (hs/2) - (h/2)

    # set the dimensions of the screen
    # and where it is placed
    root.geometry('%dx%d+%d+%d' % (w, h, x, y))

    root.mainloop()
