import tkinter as tk
import urllib
from bs4 import BeautifulSoup

class App:
    def __init__(self, root):
        self.root = root
        for text in ("http://google.com", "http://facebook.com", "https://www.youtube.com/watch?v=oozQ4yV__Vw"):
            link = tk.Label(text=text, foreground="#0000ff")
            link.bind("<1>", lambda event, text=text: self.click_link(event, text))
            link.pack()

    def click_link(self, event, text):

        soup = BeautifulSoup(urllib.request.urlopen(text))
        print (soup.title.string)
        print("You clicked '%s'" % text)

root = tk.Tk()
app = App(root)
root.mainloop()
