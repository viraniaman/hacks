#!/usr/bin/env python
from bs4 import BeautifulSoup
import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import sendgrid
from sendgrid.helpers.mail import *
import logging
from logging.handlers import RotatingFileHandler

# settings for mail server and other script related
server = None
sg = None

dir_path = os.path.dirname(os.path.realpath(__file__))
mail_type = "insti"
#end

#settings for logging
log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
log_file = os.path.join(dir_path, "main.log")

my_handler = RotatingFileHandler(log_file, mode='a', maxBytes=50*1024*1024,
                                 backupCount=2, encoding=None, delay=0)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.INFO)

app_log = logging.getLogger('root')
app_log.setLevel(logging.INFO)

app_log.addHandler(my_handler)
#end

def connect_mailserver():
    global server
    global mail_type
    global sg
    # try:
    #     server = smtplib.SMTP('smtp-auth.iitb.ac.in', 25, timeout=20)
    #     server.ehlo()
    #     server.starttls()
    #     server.login("sandesh_patil@iitb.ac.in", "sanikapatil@")
    # except Exception:
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    mail_type = "sendgrid"
    app_log.info("Mail server connected")

def send_mail(email_id, subject, text):

    fromaddr = 'placement_blog@placements.iitb.ac.in'
    msg = MIMEMultipart('alternative')
    msg['From'] = fromaddr
    msg['To'] = email_id
    msg['Subject'] = subject
    msg.attach(MIMEText(text, 'html'))

    if(mail_type is "insti"):
        server.sendmail(fromaddr, email_id, msg.as_string())
    else:
        from_email = Email(fromaddr)
        to_email = Email(email_id)
        app_log.info("Sending '"+subject+"' mail to: "+str(email_id))
        content = Content("text/html", text)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        app_log.info("Mail response status code is "+str(response.status_code))

def clear_metadata(samples):
    for s in samples:
        metadata = s.find(class_='postmetadata')
        metadata.clear()

def difference(samples, samples_prev):

    a, b = {}, {}
    app_log.info("Now inside finding difference")
    for idx, val in enumerate(samples):
        entry = val.find(class_='entry')
        a["".join(filter(str.isalpha, entry.get_text()))] = idx

    app_log.info("Entries in new webpage are: "+str(a))

    for idx, val in enumerate(samples_prev):
        entry = val.find(class_='entry')
        b["".join(filter(str.isalpha, entry.get_text()))] = idx

    app_log.info("Entries in old webpage are: "+str(b))

    diff = set(list(a)) - set(list(b))

    app_log.info("difference between the two found is: "+str(list(diff)))

    c = []

    for elem in diff:
        c.append(samples[a[elem]])

    app_log.info("Returning "+str(c)+" as the difference")
    return c

def text_formatter(elem):

    # iterate through all tags inside the post and reformat all strings to
    # match the following format
    # the beginning of a sentence must be Uppercase followed by lowercase letters everywhere else

    text = (elem.string)
    sentences = str(text).split('.')
    sentences_fixed = []

    for sentence in sentences:
        sentence = sentence.strip()
        if(sentence):
            s1 = sentence[0].upper()
            s2 = sentence[1:].lower()
            sentences_fixed.append(s1+s2)

    sentence_string = ""

    for sentence in sentences_fixed:
        sentence_string += sentence + ". "

    elem.string = sentence_string

    app_log.info("Formatted the string to: "+sentence_string)

if(__name__ == "__main__"):

    # test_text_formatter()

    r = requests.get('http://140070009:04041996@iit^@placements.iitb.ac.in/blog/')
    if(r.status_code == 200):

        app_log.info("Got the web page")
        content = r.content
        soup = BeautifulSoup(content, 'lxml')
        samples = soup.find_all(class_ = 'post')
        clear_metadata(samples)

        with open(os.path.join(dir_path, 'prev_posts.html'), 'r+') as f:
            soup_prev = BeautifulSoup(f.read().rstrip(), 'lxml')
            samples_prev = soup_prev.find_all(class_='post')
            clear_metadata(samples_prev)

            diff = difference(samples, samples_prev) #returns the new html posts

            app_log.info("Found number of differences = "+str(len(diff)))

            if(len(diff)>0):
                #send mail for each element in diff
                connect_mailserver()
                with open(os.path.join(dir_path, 'email_ids.txt')) as id_file:

                    ids = id_file.read().splitlines()
                    #for all elements in diff, send emails to all ids
                    for elem in diff:

                        text_formatter(elem.h2)
                        # entry = elem.find(class_='entry')
                        # entry_elements = entry.descendants
                        # for element in entry_elements:
                        #     text_formatter(element)

                        email_subject = '%s' % elem.h2.get_text()
                        title = elem.h2
                        title.clear()
                        date = elem.small
                        date.clear()

                        email_content = """<html><head></head><body>"""

                        email_content += str(elem)

                        email_content += """</html></body>"""

                        # print(email_content)
                        # print()

                        for id in list(set(ids)):
                            send_mail(id, email_subject, email_content)

                pass

                #write to prev_posts file
                f.seek(0)
                f.truncate()
                f.write(r.text)


    else:
        app_log.info('Could not contact server! Please try again')
        send_mail('viraniaman@gmail.com', 'Placement Blog Script Down!', 'Check the script')
