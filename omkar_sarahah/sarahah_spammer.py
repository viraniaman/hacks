import os
from selenium import webdriver
import time
from logconf import app_log

dir_path = os.path.dirname(os.path.realpath(__file__))
galis = 'obscenities.txt'
links = 'links.txt'

def set_viewport_size(driver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    driver.set_window_size(*window_size)

with open(os.path.join(dir_path, galis)) as words:

    word_list = words.read().splitlines()
    #set_viewport_size(driver, 1366, 768)

    with open(os.path.join(dir_path, links)) as links_to_sarahah:

        link_list = links_to_sarahah.read().splitlines()

        while(True):
            for gali in word_list:
                for link in link_list:
                    driver = webdriver.PhantomJS()
                    driver.get(link)
                    # driver.save_screenshot("aaa.png")
                    text = driver.find_element_by_tag_name("textarea")
                    button = driver.find_element_by_id("Send")
                    text.send_keys(gali)
                    button.click()
                    driver.close()
                    app_log.info("sent "+str(gali)+" to "+str(link))
                    time.sleep(15)
