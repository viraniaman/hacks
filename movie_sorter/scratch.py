import requests
from bs4 import BeautifulSoup
import re
import math
import operator

def get_and_extract(url, tag_selector):

    print("getting and extracting from "+url)
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'lxml')
    elems = soup.select(tag_selector)
    elems = [get_name_only(elem.string) for elem in elems]

    return elems

def get_name_only(st):

    # if(st is not None):
    #     if()

    if(st is not None):
        return (" ".join(re.findall("[a-zA-Z]+", st))).lower()
    else:
        return None

urls = ['http://www.imdb.com/list/ls055592025/',
        'http://www.afi.com/100Years/movies.aspx',
        'https://www.rottentomatoes.com/top/bestofrt/']

tag_selectors = ['div.list_item > div.info > b > a',
                 'label.filmTitle',
                 'a.unstyled.articleLink']

movies_table = dict()

for i, url in enumerate(urls):

    single_list = get_and_extract(url, tag_selectors[i])

    for num, e in enumerate(single_list):

        if e not in movies_table :
            movies_table[e] = []

        movies_table[e].append(num+1)

# print(movies_table)

movies_table  = {k:v for k, v in movies_table.items() if len(v) is 3}

print(movies_table)

movie_score = {}

for k, v in movies_table.items():

    sum = 1
    for elem in v:
        sum *= elem

    movie_score[k] = pow(sum, 1/len(v))

sorted_x =  sorted(movie_score.items(), key=operator.itemgetter(1))

print(sorted_x)
