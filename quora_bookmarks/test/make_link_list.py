from selenium import webdriver
from selenium.webdriver import *
import urllib
import time
import pdfkit
import pyperclip

def is_visible(locator, timeout=2):
    try:
        ui.WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        return True
    except Exception:
        return False

def link_to_text(question):

	ind2 = question.find('/answer')
	ind1 = len('https://www.quora.com/')
	return ' '.join(question[ind1:ind2].split('-'))

# wait for yes input until you're on bookmarks page
# chrome_options = ChromeOptions()
# chrome_options.add_argument("--kiosk")
# driver = webdriver.Chrome(chrome_options=chrome_options)
driver = webdriver.Chrome()
driver.get('https://www.quora.com/bookmarked_answers?order=desc')

uname = "viraniaman313@gmail.com"
pwd = "''aman7666''"

login = driver.find_elements_by_class_name('header_login_text_box')

login[0].clear()
login[1].clear()

login[0].send_keys(uname)
login[1].send_keys(pwd)

driver.find_element_by_xpath('//input[@value="Login"]').click()

time.sleep(5)

question = driver.find_element_by_class_name('feed_item')
last_question = question.find_element_by_class_name('question_text').text

print("last q is "+str(last_question))

driver.get('https://www.quora.com/bookmarked_answers')

questions = driver.find_elements_by_class_name('feed_item')
question = questions[-1].find_element_by_class_name('question_text').text

while(str(question) != str(last_question)):
	# questions = questions_new
	# question = questions[-1].find_element_by_class_name('question_text').text
	driver.execute_script('window.scrollTo(0,document.body.scrollHeight);')
	time.sleep(2)
	questions = driver.find_elements_by_class_name('feed_item')
	question = questions[-1].find_element_by_class_name('question_text').text
	# questions_new = driver.find_elements_by_class_name('feed_item')
	# question_new = questions_new[-1].find_element_by_class_name('question_text').text

	# print("question is "+str(question))
	# print("question_new is "+str(question_new))

# click on all more links

# generating all links by clicking on the share button copy link

share_buttons = driver.find_elements_by_class_name('hover_menu')

for button in share_buttons:
	driver.execute_script('return arguments[0].scrollIntoView();', button)
	driver.execute_script('window.scrollBy(0,-150);')
	button.click()
	items = driver.find_elements_by_class_name('menu_list_item')
	items[0].click()
	linktext = pyperclip.paste()
	pdfkit.from_url(linktext, str(link_to_text(linktext))+".pdf")

# more_links = driver.find_elements_by_class_name('more_link')

# for link in more_links:

# 	driver.execute_script('return arguments[0].scrollIntoView();', link)
# 	driver.execute_script('window.scrollBy(0,-150);')

# 	if(is_visible(link.get_attribute('class'), 5)):
# 		link.click()

# source = driver.page_source

# pdfkit.from_url(source, 'bookmarks.pdf')
